import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {

  constructor() { 
  }

  ngOnInit() {
  this.username = "Roberto";
  this.userlastname = "Sánchez";
  this.usermail = "roberto@gmail.com";
  }

}
